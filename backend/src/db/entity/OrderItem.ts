import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne } from "typeorm";
import { Order } from "./Order";
import { ItemUnit } from "./ItemUnit";

@Entity()
export class OrderItem {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => ItemUnit)
    itemUnit: ItemUnit;

    @Column()
    amount:number

    @ManyToOne(type => Order)
    order: Order;
}
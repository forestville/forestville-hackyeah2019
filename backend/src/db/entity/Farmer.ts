import {Entity, PrimaryGeneratedColumn, Column, OneToMany, OneToOne, JoinColumn} from "typeorm";
import { Area } from "./Area";
import { UserBase } from "./UserBase";

@Entity()
export class Farmer{

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type=>UserBase, {eager: true})
    @JoinColumn()
    user:UserBase

    @OneToMany(type=>Area,area=>area.farmer)
    areas:Area[]

    @Column({ nullable: true })
    description: string;

    @Column({ nullable: true })
    bannerImageUrl: string;
}

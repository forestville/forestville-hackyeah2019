import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToOne, OneToMany } from "typeorm";
import { UserBase } from "./UserBase";
import { OrderItem } from "./OrderItem";

@Entity()
export class Receipt {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({default:0})
    price: number;

    @Column({default:0})
    sumOfItems:number;

    @Column()
    vat: number;

    @Column()
    status: string;

}

export const ReceiptStatus={
    TO_BE_PAID:"TO_BE_PAID",
    PENDING:"PENDING",
    PAID:"PAID "
}
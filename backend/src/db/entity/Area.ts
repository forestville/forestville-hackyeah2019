import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from "typeorm";
import { Farmer } from "./Farmer";
import { ItemUnit } from "./ItemUnit";

@Entity()
export class Area {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("double precision")
    longitude: number;

    @Column("double precision")
    latitude: number;

    @Column()
    shape_data: string;

    @Column({nullable:true})
    description: string;

    @Column()
    name: string;

    @Column()
    address: string;

    @Column({nullable:true})
    bannerImageUrl: string;

    @ManyToOne(type => Farmer, {eager: true})
    farmer: Farmer;

    @OneToMany(type=>ItemUnit,itemUnits=>itemUnits.area, {eager: true})
    itemUnits: ItemUnit[];
}

import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";

@Entity()
export class Item {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({default:"tree"})
    type: string;

    @Column()
    imageUrl: string;

    @Column()
    thumbnailImageUrl: string;
}
import {Entity, PrimaryGeneratedColumn, Column, OneToOne, JoinColumn} from "typeorm";
import { Farmer } from "./Farmer";
import { Donator } from "./Donator";

@Entity()
export class UserBase {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: false,default:1 })
    authLevel: number;

    @Column({ nullable: true })
    firstName: string;

    @Column()
    email: string;

    @Column({select:false})
    passwordHash: string;

    @Column({ nullable: true,select:false })
    refreshToken: string;

    @Column({ nullable: true })
    lastName: string;

    @Column({ nullable: true })
    avatarImageUrl: string;

    @Column({ default: 0 })
    accountBalance: number;

    @OneToOne(type=>Farmer,{cascade:true})
    @JoinColumn()
    farmer:Farmer

    @OneToOne(type=>Donator,{cascade:true})
    @JoinColumn()
    donator:Donator
}

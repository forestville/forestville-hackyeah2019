import {getRepository} from 'typeorm'
import { Farmer } from '../entity/Farmer';

export async function saveFarmer(farmer:Farmer):Promise<Farmer>{
    return await getRepository(Farmer).save(farmer);
}

export async function createFarmer(user:Farmer|object):Promise<Farmer>{
    return await getRepository(Farmer).create(user);
}

export async function getFarmerById(id:number):Promise<Farmer>{
    return await getRepository(Farmer).findOne(id,{relations:["user"]});
}

export async function getAllFarmers():Promise<Farmer[]>{
    return await getRepository(Farmer).find();
}
import {getRepository, DeleteResult} from 'typeorm'
import { OrderItem } from '../entity/OrderItem';

export async function saveOrderItem(orderitem:OrderItem):Promise<OrderItem>{
    return await getRepository(OrderItem).save(orderitem);
}

export async function getOrderItemById(id:number):Promise<OrderItem>{
    return await getRepository(OrderItem).findOne({id:id});
}

export async function getAllOrderItems():Promise<OrderItem[]>{
    return await getRepository(OrderItem).find();
}

export async function deleteOrderItemById(id:number):Promise<DeleteResult>{
    return await getRepository(OrderItem).delete(id);
}
import {getRepository, DeleteResult} from 'typeorm'
import { Item } from '../entity/Item';

export async function saveItem(item:Item):Promise<Item>{
    return await getRepository(Item).save(item);
}

export async function getItemById(id:number):Promise<Item>{
    return await getRepository(Item).findOne({id:id});
}

export async function getAllItems():Promise<Item[]>{
    return await getRepository(Item).find();
}

export async function deleteItemById(id:number):Promise<DeleteResult>{
    return await getRepository(Item).delete(id);
}
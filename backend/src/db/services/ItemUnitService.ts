import {getRepository, DeleteResult} from 'typeorm'
import { ItemUnit } from '../entity/ItemUnit';

export async function saveItemUnit(itemunit:ItemUnit):Promise<ItemUnit>{
    return await getRepository(ItemUnit).save(itemunit);
}

export async function getItemUnitById(id:number):Promise<ItemUnit>{
    return await getRepository(ItemUnit).findOne(id,{relations:["area","item"]});
}

export async function getAllItemUnits():Promise<ItemUnit[]>{
    return await getRepository(ItemUnit).find();
}

export async function deleteItemUnitById(id:number):Promise<DeleteResult>{
    return await getRepository(ItemUnit).delete(id);
}
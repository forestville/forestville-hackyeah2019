import { Application, Request } from "express";
import { Response } from "express-serve-static-core";
import { NextFunction } from "connect";
import { SignIn, SignUpFarmer, SignUpDonator, refreshAccessToken } from "../middlewares/passport/authenticate";
import {classToPlain} from "class-transformer";
async function login(req:Request,res:Response,next:NextFunction){
    res.json({user:classToPlain(res.locals.user),jwt:res.locals.jwt});
}

async function registerFarmer(req:Request,res:Response,next:NextFunction){
    res.json({user:classToPlain(res.locals.user)});
}

async function registerDonator(req:Request,res:Response,next:NextFunction){
    res.json({user:classToPlain(res.locals.user)});
}

async function handleRefreshAccessToken(req:Request,res:Response,next:NextFunction){
    res.json({jwt:res.locals.jwt});
}

module.exports = (app:Application)=>{
    
    app.post('/auth/login',SignIn,login);
    app.post('/auth/register/farmer',SignUpFarmer,registerFarmer);
    app.post('/auth/register/donator',SignUpDonator,registerDonator);
    app.post('/auth/refreshaccesstoken',refreshAccessToken,handleRefreshAccessToken);
}
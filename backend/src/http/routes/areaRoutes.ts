import { Application, Request } from "express";
import { Response } from "express-serve-static-core";
import { getAreaById, getAllAreas, saveArea, getAreaWithUnitIds } from "../../db/services/AreaService";
import { saveItemUnit } from "../../db/services/ItemUnitService";
import { isLoggedIn, authLevel } from "../middlewares/passport/authenticate";
import { userInfo } from "os";
import { saveUser, getDeepUser } from "../../db/services/UserService";
import { UserBase } from "../../db/entity/UserBase";
import { saveFarmer } from "../../db/services/FarmerService";
import { getItemById } from "../../db/services/ItemService";
import { ItemUnit } from "../../db/entity/ItemUnit";
import { mustBeFarmer } from "../middlewares/security/user";

async function findAreaById(req: Request, res: Response) {
    const area = await getAreaById(Number(req.params.id));
    res.json({ area });
}

async function getNearestArea(req: Request, res: Response,next) {//TODO
    //const areas = await getAreasOrderedByDistance(long, lat, radius);
    let includedItems = null;
    if (req.query.includedItems != null) {
        if (typeof(req.query.includedItems) === "string") {
            includedItems = [parseInt(req.query.includedItems)];   
        } else {
            includedItems = req.query.includedItems.map((data) => parseInt(data));
        }
    }
    
    const areas = await getAllAreas();
    // This is not how you do filtering :=)
    const areasDTOs = areas.filter((area) => {
        if (includedItems == null) {
            return true;
        }

        if (area.itemUnits != null) {
            const countFound = area.itemUnits.filter(item => {{
                for (var ii in includedItems) {
                    if (includedItems[ii] === item.id) {
                        return true;
                    }
                    return false;
                }
            }}).length;
            return countFound > 0;
        }

        return false;
    }).map((area, ix) => {
        area["distance"] = ix;
        area["rank"] = ix;
        delete area["longitude"];
        delete area["latitude"];
        return area;
    });

    res.json(areasDTOs);
}

async function postItemUnit(req: Request, res: Response,next) {
    const area = await getAreaWithUnitIds(Number(req.params.id));
    const item = await getItemById(req.body.itemId);
    var unitItem = new ItemUnit();
    unitItem.price=req.body.price;
    unitItem.urgencyFactor=req.body.urgencyFactor;
    unitItem.area=area;
    unitItem.item=item;
    unitItem = await saveItemUnit(unitItem);
    return res.json(unitItem);
}

async function postArea(req: Request, res: Response,next) {
    const itemData = req.body.area;
    const area = await saveArea(itemData);
    var user:UserBase = await getDeepUser(res.locals.user.id);
    user.farmer.areas.push(area);
    user =await saveUser(user);
    return res.json({areas:user.farmer.areas});
}

module.exports = (app: Application) => {
    app.post('/area',isLoggedIn(authLevel.USER), mustBeFarmer, postArea);
    app.post('/area/:id/item',isLoggedIn(authLevel.USER),mustBeFarmer, postItemUnit);
    app.get('/area/:id', findAreaById);

    app.get('/search/area', getNearestArea);
}
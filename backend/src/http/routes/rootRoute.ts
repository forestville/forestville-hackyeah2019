import { Application, Request } from "express";
import { Response } from "express-serve-static-core";
import { NextFunction } from "connect";
import { getUserById } from "../../db/services/UserService";
import { isLoggedIn, authLevel } from "../middlewares/passport/authenticate";

async function rootRoute(req:Request,res:Response,next:NextFunction){
    res.json({message:"It works"});
}

async function testPost(req:Request,res:Response,next:NextFunction){
    res.json(req.body);
}

module.exports = (app:Application)=>{
    app.get('/home',isLoggedIn(authLevel.GUEST),rootRoute);
    app.post('/testpost',isLoggedIn(authLevel.GUEST),testPost);
}
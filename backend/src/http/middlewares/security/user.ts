import { NextFunction } from "connect";
import { Response, Request } from "express";
import { CustomError } from "../../../utils/errors/customError";

export async function mustBeFarmer(req:Request,res:Response,next:NextFunction){
    if(res.locals.user.farmer)return next();
    return next(new CustomError("user must be farmer",500));
}

export async function mustBeDoner(req:Request,res:Response,next:NextFunction){
    if(res.locals.user.donator)return next();
    return next(new CustomError("user must be doner",500));
}
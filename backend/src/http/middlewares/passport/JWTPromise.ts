let jwt=require("jsonwebtoken");
const jwtSecret= "qwertyuiopdfghjklcvbnm,"
class JWTPromise{
  constructor(){

  }
  async sign(data){
    return await new Promise((resolve,reject)=>{
      jwt.sign(data, jwtSecret,{ expiresIn: 60 * 30 }, function(err, token) {
        if(err)reject(err);
        else resolve(token);
      });
    }).catch(err=>{throw err});
  }
  async verify(token){
    return await new Promise((resolve,reject)=>{
      jwt.verify(token,jwtSecret,function(err,decoded){
        if(err){
          reject("invalid access token");
        }else{
          resolve(decoded);
        }
      });
    }).catch(err=>{throw err});
  }
}
var jwtObj = new JWTPromise();
export default jwtObj;

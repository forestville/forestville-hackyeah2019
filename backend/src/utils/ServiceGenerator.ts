import * as readline from 'readline'
import { writeFileSync } from 'fs';

var answer = process.argv[2].split("=")[1];
 
const serviceNameLowerCase = answer.toLowerCase();
const serviceNameUpperCase = answer[0].toUpperCase()+answer.slice(1);

const serviceText=`import {getRepository, DeleteResult} from 'typeorm'
import { ${serviceNameUpperCase} } from '../entity/${serviceNameUpperCase}';

export async function save${serviceNameUpperCase}(${serviceNameLowerCase}:${serviceNameUpperCase}):Promise<${serviceNameUpperCase}>{
    return await getRepository(${serviceNameUpperCase}).save(${serviceNameLowerCase});
}

export async function get${serviceNameUpperCase}ById(id:number):Promise<${serviceNameUpperCase}>{
    return await getRepository(${serviceNameUpperCase}).findOne({id:id});
}

export async function getAll${serviceNameUpperCase}s():Promise<${serviceNameUpperCase}[]>{
    return await getRepository(${serviceNameUpperCase}).find();
}

export async function delete${serviceNameUpperCase}ById(id:number):Promise<DeleteResult>{
    return await getRepository(${serviceNameUpperCase}).delete(id);
}`;

writeFileSync(`./src/db/services/${serviceNameUpperCase}Service.ts`,serviceText);

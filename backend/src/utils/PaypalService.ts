const request = require('request');

export class PaypalService {
    
    createOrder(price: number, internalOrderId: number) {
        const data = {
            "intent": "CAPTURE",
            "purchase_units": [
                {
                    "amount": {
                        "currency_code": "USD",
                        "value": price
                    },
                    "invoice_id": internalOrderId
                }
            ],
            return:`http://localhost:8080/paypal/${internalOrderId}/complete`
        }

        var options = {
            url: 'https://api.sandbox.paypal.com/v2/checkout/orders',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer A21AAH4m8YCka7TdPXwpa1Emy8JLZJATxda1qn0xrmGTeMtbnevfh7L7beWKUFQ_BCraGXbkzXtA-N7OjwzmvVHDRyDHvOaVA'
            }
          };
        
        
          return new Promise<any>((resolve, reject) => {
            request.post(options, function(error, response, body) {
                if (error) {
                    return reject(error);
                }

                return resolve(JSON.parse(body));
            });
        })
        
    }

    getOrder(orderId: number): Promise<any> {
        var options = {
            url: 'https://api.sandbox.paypal.com/v2/checkout/orders/' + orderId,
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer A21AAH4m8YCka7TdPXwpa1Emy8JLZJATxda1qn0xrmGTeMtbnevfh7L7beWKUFQ_BCraGXbkzXtA-N7OjwzmvVHDRyDHvOaVA'
            }
          };
        
        
        return new Promise<any>((resolve, reject) => {
            request.get(options, function(err, resp, body) {
                if (err) {
                    return reject(err);
                }

                return resolve(JSON.parse(body));
            });
        })
    }
}
﻿import {CustomError} from './customError'

export default {
  userAlreadyExists: new CustomError("This user already exists", 409),
  couldntCreateUser: new CustomError("Couldn't create user", 500),
  
  userFromSessionDoesntexists: new CustomError("Couldn't find user who's id is stored in session", 400),
  invalidUsernameOrPassword: new CustomError("invalid username or passowrd", 401),
  invalidPassword: new CustomError("invalid passowrd", 401),
  userUnauthenticated: new CustomError("user not authenticated", 401),

  userUnauthorized: new CustomError("user is not authorized for this action", 403),
  invalidRefreshToken: new CustomError("Invalid refresh token", 409),
  invalidAccessToken: new CustomError("Invalid access token", 401),
  userDoesntExist: new CustomError("user doesn't exist", 404),

}
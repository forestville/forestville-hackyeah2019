import * as md5 from 'md5'
export function hashPassword(password: string) {
  return md5(password);
}

export function isValid(password,passwordHash) {
  return md5(password)===passwordHash;
}
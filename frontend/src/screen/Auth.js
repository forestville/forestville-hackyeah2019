import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Form, Row, Col, Input, Icon, Button, Radio, message } from 'antd'
import { connect } from 'react-redux'
import { makeAction } from '../redux/actions/makeAction'
import Navbar from '../component/Navbar'


const Auth = props => {
    const [loading, setLoading ] = useState(false)
    const { getFieldDecorator } = props.form;
    const login = (values) => {
        setLoading('signin')
        props.login({
          creds: {
              email:values.email,
              password:values.password
          },
          cb: ({user}) => {
            setLoading(false)
            if(user){
              props.history.push('/')
              return
            }
            message.error("Something went wrong. Try again.")
          }
        })
      }
      const signup = (values) => {
       setLoading('signup')
        props.register({
            creds: {
                email:values.email,
                password:values.password,
                role: 'donator'
            },
          cb: ({user}) => {
            setLoading(false)
            if(user){
              message.success(`You successfully signed up`)
              props.history.push('/')
              return
            }
            message.error("Something went wrong")
          }
        })
      }
    const submit = (action) => {
        props.form.validateFields((err, values) => {
            if (!err) {
              if(action === 'signup'){
                  signup(values)
                  return
              }
              login(values)
            }
          })
    }
    return (
      <>
      <Navbar />
        <div className="auth-page">
            <Row>
                <Col span={12}>
                    <div className="auth-page-image-container" />
                </Col>
                <Col span={12}>
                    <div className="auth-page-form-wrapper">
                    <div className="container container-narrow">
                    <Form className="auth-form" layout="vertical">
        <Form.Item label="Email">
          {getFieldDecorator('email', {
            rules: [{ required: true, message: 'Please input your email' }],
          })(
            <Input
              prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
             size="large"
            />,
          )}
        </Form.Item>
        <Form.Item label="Password">
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password' }],
          })(
            <Input
              prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="password"
             size="large"
            />,
          )}
        </Form.Item>
      </Form>
      <Row gutter={20} className="auth-page-actions">
          <Col span={12}>
          <Form.Item>
          {getFieldDecorator('signin')(
            <Button
            type="primary"
            size="large"
            block
            className="sign-up"
            htmlType="submit"
            loading={loading === 'signin'}
            onClick={() => submit('signin')}
            disabled={loading}>Sign in</Button>
          )}
        </Form.Item>
             
          </Col>
          <Col span={12}>
          <Form.Item>
          {getFieldDecorator('signup')(
            <Button
            size="large"
            block
            className="sign-up"
            htmlType="submit"
            loading={loading === 'signup'}
            disabled={loading}
            onClick={() => submit('signup')}
            >Sign up</Button>
          )}
        </Form.Item>
              
          </Col>
      </Row>
  
                    
                    </div>
                    </div>
               
                </Col>
            </Row>
            
        </div>
        </>
    )
}
const mapDispatchToProps = {
    login: makeAction('LOGIN_REQUESTED'),
    register: makeAction('REGISTER_REQUESTED')
  }
  
  export default connect(null, mapDispatchToProps)(withRouter(Form.create()(Auth)))
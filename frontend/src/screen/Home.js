import React, {useState} from 'react'
import Navbar from '../component/Navbar'
import {Button, Col, Row, Icon} from 'antd'
import {Link} from 'react-router-dom'
import LandingPageMap from '../component/LandingPageMap'



export default props =>  (
        <>
         <Navbar />
            <header className="home-header" id="home">
            <div className="home-header-container">
            <h1>Join Forest<span className="brand-green">ville</span></h1>
            <h3>Let us help you take care of the trees.</h3>
            <Button type="primary" size="large">
                <Link to="/auth">Join us</Link>
            </Button>
            </div>
            </header>
            <section className="home-first-section" id="importance-of-trees">
                <div className="container">
                <Row>
                <Col span={12}>
                <Icon type="exclamation-circle" theme="twoTone" twoToneColor="#1DA57A" style={{
                    fontSize: '10vh',
                    textAlign: 'center',
                    position: 'relative',
                    left:'50%',
                    transform: 'translate(-50%, 0)'
                }}/>
                </Col>
                     <Col span={12}>
                         <h2>Why trees are so important for the environment?</h2>
                            <p>
                            Trees help clean the air we breathe, filter the water we drink, 
                            and provide habitat to over 80% of the world's terrestrial biodiversity.
                             Forests provide jobs to over 1.6 billion people, absorb harmful carbon from the atmosphere,
                              and are key ingredients in 25% of all medicines. 
                            Ever taken an Aspirin? It comes from the bark of a tree!
                        </p>
                    </Col>
                   
                 </Row>
                
                </div>
            </section>
            <section className="home-second-section" id="our-mission">
                <div className="container">
                <Row>
                     
                  
                    <Col span={24}>
                         <h2>Our mission</h2>
                    
                            <p>
                            Decades of exploitation have destroyed and degraded much of the Earth’s natural forests.
                             In fact, we’ve already lost half of global forest land. Losing these vital ecosystems is 
                             displacing communities, threatening the habitats of rare and endangered species, 
                             and spewing greenhouse gases into the atmosphere. 
                            To avoid further consequences, we urgently need to protect what is left.
                        </p>
                        <p>
                            <b>
                            Our mission is to make it simple for anyone to help the environment by planting trees
                            </b>
                            </p>
                    </Col>
                  
                 </Row>
                
                </div>
            </section>
            <section className="home-page-map-section" id="planted-trees">
                <h2 className="home-page-map-section-title">Planted trees</h2>
                
                <LandingPageMap />
            </section>
            <section className="home-joinus-section" id="help-the-planet">
                <h2>Don't wait</h2>
                <Button size="large"  type="primary">
                    <Link to="/auth">
                    Join us today!
                    </Link>
                </Button>
            </section>

               
            
        </>
    )

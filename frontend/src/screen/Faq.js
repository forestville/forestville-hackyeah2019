import React from 'react'
import {Collapse, Icon, Row, Col} from 'antd'
const { Panel } = Collapse
const customPanelStyle = {
    // background: '#f7f7f7',
    borderRadius: 4,
    marginBottom: 24,
    border: 0,
    overflow: 'hidden',
    boxShadow: '0px 5px 15px -5px rgba(0,0,0,0.2)',
    fontSize: '1.2em'
}
const HeaderText = ({children}) => <h4 style={{fontWeight: 'bold', color:'rgba(0,0,0,0.8)'}}>{children}</h4>
const headerText = 'Who is responsible for my trees?'
  const text = 'Lorem ipsum dolor sit amet'
export default props => (
   
    <div className="container">
         <h1 style={{margin:'1em 0', color: '#1DA57A'}}>Frequently Asked Questions</h1>
        <Row>
            <Col span={12}>
            <Collapse
    bordered={false}
    defaultActiveKey={['1']}
    expandIconPosition="right"
    expandIcon={({ isActive }) => <Icon type="caret-right" style={{color:'#1DA57A', fontSize:20}} rotate={isActive ? 90 : 0} />}
  >
    <Panel header={<HeaderText>{headerText}</HeaderText>} key="1" style={customPanelStyle}>
      <p>{text}</p>
    </Panel>
    <Panel header={<HeaderText>{headerText}</HeaderText>} key="2" style={customPanelStyle}>
      <p>{text}</p>
    </Panel>
    <Panel header={<HeaderText>{headerText}</HeaderText>} key="3" style={customPanelStyle}>
      <p>{text}</p>
    </Panel>
  </Collapse>
            </Col>
        </Row>
    </div>
)
import React, {Component} from 'react'
import ForestMap from '../component/ForestMap';
import {Button, Progress, Table, Row, Col, Icon, Card, Avatar, Statistic} from 'antd';
import {selectedLocation, forestriesShapes, forestries} from '../redux/selectors/app';
import {makeAction} from "../redux/actions/makeAction";
import {SELECT_LOCATION, FORESTRIES_FETCH} from "../redux/actions/actionTypes";
import {connect} from "react-redux";
import ReactMapboxGl, {Layer, Feature, MapContext, Popup} from "react-mapbox-gl";
import _ from 'lodash'
import {polygonCenter, calcPolygonArea} from '../service/Util';
import {Link} from 'react-router-dom';
import ForestryPieChart from './ForestryPieChart';
import * as turf from '@turf/turf';

class ForestriesList extends Component {
    constructor() {
        super();
        this.state = {
            popups: null,
        }
    }

    componentDidMount() {
        this.props.fetchForestries();
        this.props.selectLocation({});
    }

    render() {
        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name'
            },
            {
                title: 'Distance',
                dataIndex: 'distance',
                key: 'distance',
            },
            {
                title: 'Address',
                dataIndex: 'address',
                key: 'address',
            },
            {
                title: 'Trees demand',
                key: 'demand',
                dataIndex: 'demand',
                render: value => (
                    <span>
                        <Progress
                            strokeColor={{
                                '0%': '#1890ff',
                                '100%': '#1DA57A',
                            }}
                            percent={Math.round(value)}
                        />
                    </span>
                ),
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <Button type={this.props.selected.key === record.key ? 'default' : 'primary'}
                                onClick={() => {
                                    const center = polygonCenter(record.shape_data)
                                    this.setState({
                                        popup: {
                                            forestry: record,
                                            lngLat: {
                                                lng: center[0],
                                                lat: center[1]
                                            }
                                        }
                                    });
                                    this.props.selectLocation(record)
                                }}>
                            <Icon type="check"/>
                        </Button>
                        </span>
                ),
            },
        ];

        const data = this.props.forestries
            .sort((a, b) => a.rank - b.rank)
            .map(forestry => ({
                ...forestry,
                key: forestry.id,
                demand: _.meanBy(forestry.itemUnits, 'urgencyFactor') * 100,
            }));


        const {popup} = this.state;
        return (
            <Row>
                <Col span={12}>
                    {
                        data.length > 0 &&
                        <ForestMap
                            center={polygonCenter(this.props.selected.shape_data || (data[0] && data[0].shape_data))}
                            cords={this.props.forestries.map(item => ({
                                id: item.id,
                                cords: item.shape_data
                            }))}
                            popups={popup && [
                                <Popup key={popup.forestry.id} coordinates={[popup.lngLat.lng, popup.lngLat.lat]}>
                                    <Card
                                        title={popup.forestry.name}
                                        style={{width: 300}}
                                        extra={<a onClick={() => {
                                            this.setState({popup: null});
                                            this.props.selectLocation({})
                                        }}>Close</a>}
                                        cover={
                                            <img
                                                alt="example"
                                                src={popup.forestry.bannerImageUrl}
                                                style={{maxHeight: 300}}
                                            />
                                        }
                                        actions={[
                                            <Button type="primary" block style={{width: "90%"}}>
                                                <Link to="/trees" style={{textDecoration: 'none', color: 'white'}}>
                                                    Plant here!
                                                </Link>
                                            </Button>,
                                        ]}
                                    >
                                        <Card.Meta
                                            title={popup.forestry.farmer.user.firstName + ' ' + popup.forestry.farmer.user.lastName}
                                            avatar={<Avatar src={popup.forestry.farmer.user.avatarImageUrl}/>}
                                            description="Forest caretaker"
                                        />
                                    </Card>
                                </Popup>
                            ]}
                            onPolygonClick={e => {
                                const forestry = _.find(this.props.forestries, {id: e.features[0].id})
                                this.setState({
                                    popup: {
                                        forestry,
                                        lngLat: e.lngLat
                                    }
                                })
                                this.props.selectLocation(_.find(data, {key: forestry.id}));
                            }}
                        />
                    }
                </Col>
                <Col span={12}>
                    <Card style={{height: 200}}>
                        {
                            !this.props.selected.id ?
                                <h1 style={{textAlign: 'center', lineHeight: '150px'}}>Choose a desired forestry</h1> :
                                <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                    <div
                                        style={{textAlign: 'center', display: 'flex', justifyContent: 'space-between', width: "100%", paddingTop: 25}}>
                                        <Statistic
                                            title={
                                                <h3 style={{display: 'inline-block'}}>Users involved</h3>
                                            }
                                            value={20}
                                            valueStyle={{color: 'green', fontWeight: 'bold', fontSize: 30}}
                                        />
                                        <Statistic
                                            title={
                                                <h3 style={{display: 'inline-block'}}>Forest type</h3>
                                            }
                                            value={"Coniferous"}
                                            valueStyle={{color: 'green', fontWeight: 'bold', fontSize: 30}}
                                        />
                                        <Statistic
                                            title={
                                                <h3 style={{display: 'inline-block'}}>Forest area</h3>
                                            }
                                            value={
                                                _.round(
                                                    turf.area({
                                                        'type': 'Feature',
                                                        'geometry': {
                                                            'type': 'Polygon',
                                                            'coordinates': [this.props.selected.shape_data]
                                                        }
                                                    }) / 10 ** 6,
                                                    1
                                                )
                                            }
                                            suffix={<span>km<sup>2</sup></span>}
                                            valueStyle={{color: 'green', fontWeight: 'bold', fontSize: 30}}
                                        />
                                    </div>
                                    <ForestryPieChart
                                        data={[
                                            {name: 'Planted', value: 400, color: "#33892b"},
                                            {name: 'Needed', value: 800, color: "#c80000"},
                                        ]}
                                    />

                                </div>
                        }

                    </Card>
                    <Table columns={columns} dataSource={data}/>
                </Col>
            </Row>
        );
    }
}

const mapStateToProps = state => ({
    selected: selectedLocation(state),
    cords: forestriesShapes(state),
    forestries: forestries(state),
});

const mapDispatchToProps = {
    selectLocation: makeAction(SELECT_LOCATION),
    fetchForestries: makeAction(FORESTRIES_FETCH)
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ForestriesList);
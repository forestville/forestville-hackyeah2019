import React from 'react'
import Dashboard from '../component/Dashboard'
import Preferences from '../component/Preferences'
import ForestriesList from '../screen/ForestriesList'
import { Card, Menu, Dropdown, Icon } from 'antd'
import {Route, Switch, Link} from 'react-router-dom'
import { makeAction } from '../redux/actions/makeAction'
import {connect} from 'react-redux'
import Trees from './Trees'

const tabList = [
    {
        key: '/',
        tab: 'Dashboard'
    },
    {
        key: '/preferences',
        tab: 'Preferences'
    },
    {
        key: '/forestries',
        tab: 'Forestries'
    }
]

const UserArea = props => (
    <>

    <Link to="/" className="user-area-brand">
    <img src={require('../assets/logo.png')} />
        <span className="brand-blue">Forest<span className="brand-green">ville</span></span>
     </Link>

    <Card
    tabList={tabList}
    tabBarExtraContent={(
        <Dropdown overlay={(
            <Menu>
          <Menu.Item key="logout" onClick={props.logout}>
            Logout
          </Menu.Item>
        </Menu>
        )}>
    <a className="ant-dropdown-link" href="#">
      <Icon type="user" /> Hi {props.user.firstName ? props.user.firstName : 'user'}!
    </a>
  </Dropdown> 
      )}
    activeTabKey={props.location.pathname}
    bordered={false}
    onTabChange={key => {
        props.history.push(key)
    }}
>
    <Switch>
        <Route path="/" exact component={Dashboard} />
        <Route path="/preferences" exact component={Preferences} />
        <Route path="/forestries" exact component={ForestriesList} />
        <Route path="/trees" exact component={Trees} />
    </Switch>
</Card>
</>
)
const mapStateToProps = state => ({
    user: state.auth.user
})
const mapDispatchToProps = {
    logout: makeAction('LOGOUT')
}
export default connect(mapStateToProps, mapDispatchToProps)(UserArea)
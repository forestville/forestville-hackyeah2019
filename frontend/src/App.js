import React from 'react';
import {Switch, Route, Redirect, withRouter} from "react-router-dom";
import {connect} from 'react-redux'
import './App.css';
import Home from "./screen/Home";
import Trees from "./screen/Trees";
import Auth from './screen/Auth'
import Faq from './screen/Faq'
import LoadingOverlay from './component/LoadingOverlay';
import UserArea from './screen/UserArea';
import OrderSuccess from "./screen/OrderSuccess";
function App(props) {
    return (
        <div className="App" >
        
            <LoadingOverlay active={props.loading} />
            <Switch>
                
                <Route path="/auth" component={Auth} />
                <Route path="/orderSuccess" component={OrderSuccess} />
                {
                    props.isLoggedIn
                    ? <Route path="/" exact component={UserArea}/>
                    : <Route path="/" exact component={Home}/>
                }
                {
                    props.isLoggedIn
                    ? (
                        <>
                        <Route path="/preferences" exact component={UserArea}/>
                        <Route path="/forestries" exact component={UserArea}/>
                        <Route path="/trees" exact={true} component={UserArea}/>
                        </>
                    )
                    : (
                    <>
                    <Route path="/trees" exact={true} render={() => <Redirect to="/" />}/>
                    <Route path="/forestries" exact render={() => <Redirect to="/" />}/>
                    <Route path="/preferences" exact render={() => <Redirect to="/" />}/>
                    </>
                    )
                }
            </Switch>
        </div>
    );
}
const mapStateToProps = (state) => ({
    isLoggedIn: state.auth.isLoggedIn,
    loading: state.auth.loading
  })
export default connect(mapStateToProps)(withRouter(App))

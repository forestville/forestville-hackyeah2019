import {all, put, select, take, takeLatest} from 'redux-saga/effects';
import request, {GET, POST} from '../../service/requests';
import * as actionTypes from '../actions/actionTypes';
import {API_URL} from "../../config";
import _ from 'lodash';
import {history} from '../../index'
import {ORDERS_FETCH} from "../actions/actionTypes";

/*
{
  type: 'CHECKOUT_TREES',
  payload: [
    {
      id: 2,
      urgencyFactor: 0.5,
      price: 1,
      item: {
        id: 1,
        name: 'Pine tree',
        type: 'tree',
        imageUrl: 'https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg',
        thumbnailImageUrl: 'https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg'
      }
    },
    {
      id: 2,
      urgencyFactor: 0.5,
      price: 1,
      item: {
        id: 1,
        name: 'Pine tree',
        type: 'tree',
        imageUrl: 'https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg',
        thumbnailImageUrl: 'https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg'
      }
    },
    {
      id: 2,
      urgencyFactor: 0.5,
      price: 1,
      item: {
        id: 1,
        name: 'Pine tree',
        type: 'tree',
        imageUrl: 'https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg',
        thumbnailImageUrl: 'https://d3bvnul2yuziy7.cloudfront.net/product-media/617/1000/1000/17488GN.jpg'
      }
    }
  ]
}
 */
function* fetchForestries() {
    while (true) {
        try {
            yield take(actionTypes.FORESTRIES_FETCH);
            const response = yield request(GET, API_URL + 'search/area');
            yield put({type: actionTypes.FORESTRIES_FETCH_SUCCESS, payload: response.data})
        } catch (e) {
            console.error(e);
            yield put({type: actionTypes.FORESTRIES_FETCH_FAILED, error: e});
        }
    }
}

function* fetchOrders() {
    while (true) {
        try {
            yield take(actionTypes.ORDERS_FETCH);
            const response = yield request(GET, API_URL + 'order');
            yield put({type: actionTypes.ORDERS_FETCH_SUCCESS, payload: response.data})
        } catch (e) {
            console.error(e);
            yield put({type: actionTypes.ORDERS_FETCH_FAILED, error: e});
        }
    }
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function* fetchRanking() {
    while (true) {
        try {
            yield take(actionTypes.RANKING_FETCH);
            const response = yield request(GET, API_URL + 'user/rankingposition');
            yield put({type: actionTypes.RANKING_FETCH_SUCCESS, payload: response.data})
        } catch (e) {
            console.error(e);
            yield put({type: actionTypes.RANKING_FETCH_FAILED, error: e});
        }
    }
}

function* checkoutOrder() {
    while (true) {
        try {
            const {payload} = yield take(actionTypes.CHECKOUT_TREES);
            const reqData = {
                itemTable: _.toPairs(_.groupBy(payload, 'id')).map(([key, value]) => ({
                    id: key,
                    quantity: value.length,
                }))
            }
            const response = yield request(POST, API_URL + 'order', reqData);
            yield put({type: actionTypes.CHECK_PAYPAL_STATUS_SUCCESS, payload: response.data});
            setTimeout(() => history.push('/orderSuccess'), 1000)
        } catch (e) {
            console.error(e);
            yield put({type: actionTypes.CHECKOUT_TREES_FAILED, error: e});
        }
    }
}

function* checkPaypal(action) {
    if(action.type !== actionTypes.CHECK_PAYPAL_STATUS) {
        return
    }
    const {payload} = action;
    const response = yield request(GET, API_URL + `order/${payload.id}`);
    if(response.data.paypal_status !== 'COMPLETE') {
        yield sleep(1000);
        yield put({type: actionTypes.CHECK_PAYPAL_STATUS, payload: response.data.order});
    } else {
        yield put({type: actionTypes.CHECK_PAYPAL_STATUS_SUCCESS, payload: response.data});
        history.push('/orderSuccess')
    }
}

export default function* appSaga() {
    yield all([
        fetchForestries(),
        checkoutOrder(),
        fetchOrders(),
        fetchRanking(),
        // takeLatest([actionTypes.CHECK_PAYPAL_STATUS, '@@router/LOCATION_CHANGE', actionTypes.CHECK_PAYPAL_STATUS_SUCCESS], checkPaypal)
    ]);
}

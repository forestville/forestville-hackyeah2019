import request, { POST, GET, PUT } from "./requests"
import jwt from 'jsonwebtoken'
const baseUrl = '/api'

export const isSessionActive = () => Boolean(localStorage.getItem('refreshToken'))

export const login = async({email, password}) => {
    const res = await request(POST, `${baseUrl}/auth/login`, {
        user: {
            email,
            password
        }
    })
    localStorage.setItem('refreshToken', res.data.user.refreshToken)
    return res.data.user
}

export const register = async({email, password, role}) => {
    const res = await request(POST, `${baseUrl}/auth/register/${role === 'donator' ? 'donator' : 'farmer'}`, {
        user: {
            email,
            password
        }
    })
    localStorage.setItem('refreshToken', res.data.user.refreshToken)
    return res.data.user
}
export const refreshAccessToken = async({refreshToken}) => {
    const res = await request(POST, `${baseUrl}/auth/refreshaccesstoken`, {
        refreshToken: refreshToken ? refreshToken : localStorage.getItem('refreshToken')
    })

    return res.data.jwt
}
export const getUser = async ({id}) => {
    const res = await request(GET, `${baseUrl}/user/${id}`)
    return res.data.user
}
export const getCurrentUser = async () => {
    if(!isSessionActive())
        throw Error('Session is not active')

    const accessToken = await refreshAccessToken({
        refreshToken: localStorage.getItem('refreshToken')
     })
     localStorage.setItem('accessToken', accessToken)
     const { id } = jwt.decode(accessToken)

     return getUser({id})
}
export const updateUserProfile = async ({data}) => {
    console.log('updating')
    if(!isSessionActive())
        throw Error('Session is not active')
  
    const res = await request(PUT, `${baseUrl}/user/updateprofile`, {
        user: data
    })
    console.log(res)
    return res.data.user
}
export const logout = () => {
    localStorage.removeItem('refreshToken')
    localStorage.removeItem('accessToken')
}



export const ShoppingCart = {
    add: (itemUnitId, quantity) => {
        const cart = ShoppingCart.getPersisted({});
        cart[itemUnitId] = quantity;
        ShoppingCart.persistCart(cart);
    },
    get: (returnOnMissing) => {
        const cartData = localStorage.getItem(ShoppingCart.SHOPPING_CART_KEY);
        if (cartData === undefined) {
            return returnOnMissing;
        }
        return JSON.parse(cartData);
    },
    isItemInCart: (itemId) => {
        return itemId in get();
    },
    removeCart: () => {
        localStorage.removeItem(ShoppingCart.SHOPPING_CART_KEY);
    },
    persistCart: (cart) => {
        localStorage.setItem(ShoppingCart.SHOPPING_CART_KEY, JSON.stringify(cart));
    },
    SHOPPING_CART_KEY = "shopping_cart"
}
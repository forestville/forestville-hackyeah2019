import React, { useState, useEffect } from 'react'
import { Icon, Button, Dropdown, Menu } from 'antd';
import AnchorLink from 'react-anchor-link-smooth-scroll'
import Scrollspy from 'react-scrollspy'
import {Link, withRouter} from 'react-router-dom'
import {connect} from 'react-redux'
import { makeAction } from '../redux/actions/makeAction';
const Navbar = (props) => {
  return (
         <nav className="navbar">
              <div className="navbar-container">
              <span className="navbar-brand">
                  <Link to="/">
                      <span>Forest</span>
                      ville
                  </Link>
              </span>
  <div className="navbar-menus">
          <Scrollspy
          items={ ['home', 'our-mission', 'importance-of-trees', 'planted-trees'] }
          componentTag={({children}) => (
            <Menu selectedKeys={[props.location.pathname === '/' ? window.location.hash.slice(1) : '']} mode="horizontal" className="navbar-menu">
              {children}
            </Menu>
          )}
          currentClassName="ant-menu-item-selected">
          <Menu.Item key="home">
            <Icon type="home" />
            
            {
                 props.location.pathname === '/' ? <AnchorLink href="#home">Home</AnchorLink>
                 : <Link to="/">Home</Link>
               }
            
            
          </Menu.Item>
         
          
          <Menu.Item key="our-mission">
            <Icon type="read" />
            
            
            {
                 props.location.pathname === '/' ? <AnchorLink href="#our-mission">Our mission</AnchorLink>
                 : <Link to="/#our-mission">Our mission</Link>
               }
            
          </Menu.Item>
          <Menu.Item key="importance-of-trees">
            <Icon type="exclamation-circle" />
            
            
            {
                 props.location.pathname === '/' ? <AnchorLink href="#importance-of-trees">Importance of trees</AnchorLink>
                 : <Link to="/#importance-of-trees">Importance of trees</Link>
               }
            
          </Menu.Item>
          <Menu.Item key="planted-trees">
            <Icon type="pushpin" />
            
            
            {
                 props.location.pathname === '/' ? <AnchorLink href="#planted-trees">Planted trees</AnchorLink>
                 : <Link to="/#planted-trees">Planted trees</Link>
               }
            
          </Menu.Item>
          </Scrollspy>
            </div>
         
        <div className="navbar-actions">
          
             {
               props.location.pathname !== '/auth' && (
                <Button type="primary">
                <Link to="/auth">Sign in</Link>
              </Button>
               )
             }
         
        </div>
        </div>
        </nav>
  
        
      )
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
  isLoggedIn: state.auth.isLoggedIn
})
const mapDispatchToProps = {
  logout: makeAction('LOGOUT')
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Navbar))
import React, { Component } from 'react'
import { Card, Avatar } from 'antd';
import PropTypes from 'prop-types';
import { Tag } from 'antd';
import { Button } from 'antd';
import { InputNumber } from 'antd';

import { cssColorFromUrgencyStr } from '../service/Util';

const { Meta } = Card;

class Tree extends Component {

    constructor(props) {
        super(props);

        this.numberChange = this.numberChange.bind(this);
    }
    
    numberChange(newNumber) {
        this.props.onNumberChange && this.props.onNumberChange(newNumber); 
    }

    render() {
        const imageUrl = this.props.imageUrl;
        const title = this.props.title;
        const description = this.props.description;
        const width = this.props.width || 240;

        const numberOfItems = this.props.numberOfItems;
        const totalPrize = this.props.totalPrize;
        const urgency = this.props.urgency;

        const cardClassName = this.props.className;

        return (
            <Card
            hoverable
            className={cardClassName + " tree_component"}
            style={{ width: width }}
            cover={<img src={imageUrl} style={{width: '100%'}} />}
          >
            <Tag color="red" className="info">Urgent</Tag>
            <div className="shifter">

            <Meta avatar={<h2>1 pln</h2>} title={title} description={description} />
            <div className="content">
                <Button  icon="poweroff" type="primary">Add to shopping cart</Button>
            </div>
            </div>
          </Card>
        )
    }
}

Tree.propTypes = {
    imageUrl: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    className: PropTypes.string,
    urgency: PropTypes.string,
    onNumberChange: PropTypes.func,
    numberOfItems: PropTypes.number.isRequired,
};

export default Tree;